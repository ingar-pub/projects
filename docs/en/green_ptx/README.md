---
lang: en-US
home: true
heroText: Green PtX
tagline: Levelized Cost PtX
heroImage: /images/logos.png
heroHeight: 80
actions:
  - text: Go to App
    link: https://ingarue.santafe-conicet.gov.ar/app6
    type: primary
features:
- title: Hourly climate information
  details: Detailed analysis of generation per hour
- title: Product delivery
  details: Complete End-to-end analysis
- title: Uniform / Non uniform
  details: Differentiate generation under non-uniform climatic conditions
- title: Yearly
  details: Analyze the entire year in detail
- title: Economic completion
  details: Analyze all the economic aspects involved
- title: Storage
  details: Energy storage analysis (compressed hydrogen gas)
lastUpdated: false
contributors: false
footer: Made in Santa Fe, Argentina | Copyright © 2024 INGAR
---
# Green PtX

Welcome to Green PtX, the all-in-one platform for analyzing investments and returns in the realm of green energy. With Green PtX, you can effortlessly calculate the potential profitability and environmental impact of your investments in renewable energy projects.

## Levelized Cost PtX

Our comprehensive suite of tools allows you to input various parameters such as initial investment, projected energy output, operating costs, and government incentives to generate accurate forecasts of your returns over time.

## Scientific Papers that support our work

For more information about the calculation process, refer to <a href="https://doi.org/10.1016/j.ijhydene.2023.12.052" target="_blank">Green hydrogen levelized cost assessment from wind energy in Argentina with dispatch constraints</a> publication.

::: tip INFO
Whether you're considering solar, wind, hydroelectric, or other forms of green energy, Green PtX provides you with the insights and data-driven analysis you need to make informed investment decisions that align with your financial goals and sustainability objectives. Join Green PtX today and embark on a journey towards a greener and more prosperous future.
:::

## Tools available and in development

### 1. LCOH gas – Levelized cost of Hydrogen

Our advanced tool empowers you to accurately optimize the levelized cost of hydrogen gas production, assess required investments, and evaluate operational expenses. Gain deeper insights into your project's efficiency by analyzing capacity factors and the replacement cycle of electrolyzers. Simply input the specific location of your installation and the CAPEX and OPEX of your system components, and let our tool do the rest. Streamline your decision-making process and enhance the success of your green hydrogen initiatives.

#### 1.1. LCOH liq – Levelized cost of liquid hydrogen

Take your hydrogen production analysis to the next level with our tool designed specifically for optimizing the levelized cost of liquid hydrogen. Assess necessary investments, operational costs, and optimize your project's overall efficiency. By inputting key details such as the specific location of your facility and the CAPEX and OPEX of your system components, you’ll gain valuable insights into the economics of liquid hydrogen production. Simplify your decision-making process and ensure the financial success of your liquid hydrogen projects.

#### 1.2 LCOH unif – Levelized cost of hydrogen (gas or liquid) at uniform delivery

Ensure consistency and reliability with our tool that optimizes the levelized cost of hydrogen—whether gas or liquid—delivered uniformly. This feature allows you to accurately assess the cost implications of maintaining a steady and uniform supply, considering necessary investments, operational expenses, and replacement cycles. The system can also compensate for renewable energy shortfalls by leveraging a limited grid connection or lithium-ion battery backup. Input the specific location, CAPEX, and OPEX of your system components, and receive precise calculations that support informed decision-making. Achieve consistent delivery and optimize the economic performance of your hydrogen projects.

### 2. LCOA – Levelized cost of Ammonia

Optimize your ammonia production projects with our comprehensive tool, specifically designed to calculate the levelized cost of ammonia. This tool covers the entire production process, including nitrogen generation through Air Separation Units (ASU) and intermediate storage of hydrogen and nitrogen gases. The Haber-Bosch production plant operates with a given turndown capacity and ramp up/down, for the appropriate plant operation and ensuring flexibility and efficiency. Additionally, the system can compensate for renewable energy shortfalls by utilizing a limited grid connection or lithium-ion battery backup. Input your facility's location, turndown, ramps, CAPEX, and OPEX for a precise cost analysis, enabling you to optimize ammonia delivery, whether for pipeline distribution or other applications. Make informed decisions and drive the success of your ammonia projects with confidence.

#### 2.1. LCOA – Levelized cost of Ammonia at uniform delivery

Streamline your ammonia production and export operations with our sophisticated tool designed to optimize the levelized cost of ammonia delivered uniformly. This feature is ideal for scenarios such as ammonia dispatch on transoceanic ships for export. Our tool delivers a comprehensive analysis by considering variations in production and delivery throughout the year including nitrogen production through Air Separation Units (ASU) and intermediate storage of hydrogen and nitrogen gases. The Haber-Bosch production plant operates with a given turndown capacity and ramp up/down, for the appropriate plant operation, allowing for flexible adjustments to meet consistent delivery requirements. Manage renewable energy fluctuations with a limited grid connection or lithium-ion battery backup for continuous operation. By entering your facility’s location, turndown, ramps, CAPEX, and OPEX, you’ll obtain precise calculations of the costs associated with uniform ammonia delivery, ensuring reliable and cost-effective export operations. Optimize your supply chain and enhance the profitability of your ammonia export ventures with our detailed cost analysis.

### 3. LCOMeOH – Levelized cost of Methanol

Minimize the levelized cost of green methanol with our advanced tool. This solution incorporates a sophisticated production setup, including a hydrogen production plant, CO2 capture from a neighboring facility, CO2 liquefaction, and intermediate storage for both hydrogen and CO2, ensuring a stable synthesis process. The methanol synthesis unit operates with a given turn down capacity and rapid dynamic response and is equipped with essential components such as multi-stage compressors, heat exchangers, evaporators, a synthesis reactor, and a distillation column. While the use of a raw methanol storage tank is not specifically modeled, its influence on the distillation process is considered in the cost analysis. By inputting your facility’s location, turn down, ramps, CAPEX, and OPEX, you’ll receive detailed cost insights, helping you optimize the financial performance of your green methanol production and drive the success of your sustainable energy initiatives.

#### 3.1. LCOMeOH – Levelized cost of Methanol at uniform delivery

Ensure consistency and reliability in your green methanol supply with our advanced tool, designed to optimize the leveled cost of methanol delivered uniformly. Ideal for applications such as bulk dispatch for export or large-scale distribution, this tool factors in a comprehensive production setup, including hydrogen production, CO2 capture and liquefaction, and intermediate storage of both hydrogen and CO2. While not specifically modeled, the tool also considers the use of a raw methanol storage tank to support the distillation process. By entering your facility's location, turn down, ramps, CAPEX, and OPEX, you’ll gain precise cost insights for uniform methanol delivery, maximizing the financial and logistical efficiency of your sustainable energy projects.

### 4. LCOeK – Levelized cost of e-Kerosene

Optimize your e-kerosene production with our advanced tool, designed to calculate the levelized cost of e-kerosene. This tool incorporates a detailed production setup, including hydrogen production, CO2 capture from a neighboring facility, CO2 liquefaction, and intermediate storage for both hydrogen and CO2 to ensure stable and economical Fischer-Tropsch (FT) process operation. The FT process offers moderate flexibility, allowing a given reduction in reactor load of its nominal capacity, which requires adequate storage capacity or grid energy to maintain operational levels. The tool also considers the necessity of a raw e-kerosene storage tank downstream of the reactor to ensure steady operation in the refining and distillation sector. It also evaluates the storage capacity required for e-kerosene to meet dispatch conditions. By inputting your facility’s location, turn down, ramps, CAPEX, and OPEX, you will receive precise cost insights, enabling you to balance peak power requirements and storage needs, optimizing the financial and operational performance of your e-kerosene production process.

#### 4.1. LCOeK – Levelized cost of e-Kerosene at uniform delivery

Ensure consistent and reliable e-kerosene supply with our advanced tool, designed to optimize the levelized cost of e-kerosene delivered uniformly. This tool is ideal for applications requiring steady delivery, such as large-scale distribution or export. It incorporates a detailed production setup that includes hydrogen production, treated CO2, and intermediate storage, critical for the stable operation of the Fischer-Tropsch (FT) process. The FT process offers moderate flexibility, allowing for a reduction in reactor load to maintain operational levels. Our tool ensures that your facility can maintain these levels with adequate storage capacity or grid energy support. It also considers the need for a raw e-kerosene storage tank downstream of the reactor to support continuous operation in the refining and distillation sectors. The tool evaluates the storage capacities required for e-kerosene to meet uniform delivery conditions. By entering your facility’s location, turn down, ramps, CAPEX, and OPEX, you’ll receive precise cost insights for uniform e-kerosene delivery, optimizing the financial and logistical performance of your sustainable energy projects.

## Who we are

Our team comprises a diverse group of engineers and scientists, united by a shared passion for advancing green energy solutions. With expertise spanning renewable energy systems, environmental science, and sustainable technology, we are dedicated to driving innovation that minimizes environmental impact while maximizing efficiency and reliability. Our multidisciplinary approach enables us to tackle complex challenges in the renewable energy sector, from solar and wind power to energy storage and smart grid technologies. We are committed to developing sustainable solutions that not only meet today’s energy needs but also pave the way for a cleaner, more resilient future.

![img](./logos_90.png)

