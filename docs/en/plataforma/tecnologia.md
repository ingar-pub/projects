# Stack de tecnología

## ⚒️ Tecnologías utilizadas

- MongoDB: Database
- Redis: For streams
- Minio: S3 compatible storage
- Aiohttp: Asynchronous HTTP Client/Server for asyncio and Python
- OpenAPI: Document & test endpoints
- Pip & venv: Python dependency management and packaging
- Docker & docker-compose : For Virtualization
- Hopeit.engine: Engine to run process and serve endpoints
- VueJS 3 + Vite: UI
